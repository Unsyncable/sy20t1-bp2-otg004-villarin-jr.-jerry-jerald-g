#pragma once

#include "Enemy.h"
#include "Weapons.h"
#include "Armor.h"

class Player
{
	Weapons weapon;
	Enemy enemy;
	Armor armor;
public:
	Player();
	virtual ~Player();
	
	void levelUp();

	void initializeWarrior(const string name);
	void initializeTheif(const string name);
	void initializeCrusader(const string name);
	void showStats() const;
	void heal();


	inline const int &getX() const { return this->xCoord;}
	inline const int &getY() const { return this->yCoord;}

	inline const string& getName() const { return this->name;}
	inline const int& getLevel() const { return this->level;}
	inline const int& getXp() const { return this->xp; }
	inline const int& getxpNext() const { return this->xpNext; }
	inline const int& getHp() const { return this->hp;}
	inline const int& gethpMax() const { return this->hpMax; }
	inline const int& getdmgMin() const { return this->damageMin; }
	inline const int& getdmgMax() const { return this->damageMax; }
	inline const int& getPow() const { return this->Pow; }
	inline const int& getDex() const { return this->Dex; }
	inline const int& getAgi() const { return this->Agi; }
	inline const int& getVit() const { return this->Vit; }

	void setxcoord(int xCoord);
	void setycoord(int yCoord);

	void attack(Enemy* target);


private:
	string name;

	int level;
	int xp;
	int xpNext;
	int hp;
	int hpMax;
	int damageMin;
	int damageMax;
	int Pow;
	int Dex;
	int Agi;
	int Vit;

	int gold;

	int xCoord;
	int yCoord;


};

