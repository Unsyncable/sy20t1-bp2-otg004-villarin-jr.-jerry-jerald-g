#pragma once
#include "Item.h"
class Weapons:
	public Item
{
public:
	Weapons(int damage = 5, string name = "Weapon1", int price = 0);
	virtual ~Weapons();

	inline const int& getdamage(){ return this->damage; };


private:
	int damage;
};

