#pragma once
#include "Item.h"
class Enemy
{
public:
	Enemy(int level = 1);
	virtual ~Enemy();

	void initializeGoblin();
	void initializeOgre();
	void initializeOrc();
	void initializeOrcLord();
	inline const int& getLevel() const { return this->level; }
	inline const int& getHp() const { return this->hp; }
	inline const int& gethpMax() const { return this->hpMax; }
	inline const int& getdmgMin() const { return this->damageMin; }
	inline const int& getdmgMax() const { return this->damageMax; }
	inline const int& getPow() const { return this->Pow; }
	inline const int& getDex() const { return this->Dex; }
	inline const int& getAgi() const { return this->Agi; }
	inline const int& getVit() const { return this->Vit; }

	void sethp(int hp);

	inline bool enemyAlive() { return this->hp > 0; }

	string getAsString()const;
	//void attack(Player* target);


private:
	int level;
	int hp;
	int hpMax;
	int damageMin;
	int damageMax;
	int Pow;
	int Dex;
	int Agi;
	int Vit;
	int goldDrop;
	int xpToGive;
};

