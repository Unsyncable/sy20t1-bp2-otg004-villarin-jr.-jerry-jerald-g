#include "Player.h"

Player::Player()
{
	this->xCoord = 0;
	this->yCoord = 0;
	this->name = "";
	this->level = 0;
	this->xp = 0;
	this->xpNext = 0;
	this->hp = 0;
	this->hpMax =0;
	this->damageMin = 0;
	this->damageMax = 0;
	this->Pow = 0;
	this->Dex = 0;
	this->Agi = 0;
	this->Vit = 0;
	this->gold = 0;
}

Player::~Player()
{
}

void Player::levelUp()
{
	while (xp >= xpNext)
	{
		this->xp -= this->xpNext;
		this->level++;
		this->xpNext = this->level * 1000;
	}
}

void Player::initializeWarrior(const string name)
{
	this->xCoord = 0;
	this->yCoord = 0;

	this->Pow = 10;
	this->Dex = 5;
	this->Agi = 5;
	this->Vit = 5;
	this->name = name;
	this->level = 1;
	this->xp = 0;
	this->xpNext = level * 1000;
	this->hp = 10;
	this->hpMax = 10;
	this->damageMin = 2;
	this->damageMax = 4;
	this->gold = 100;
}
void Player::initializeTheif(const string name)
{
	this->xCoord = 0;
	this->yCoord = 0;

	this->Pow = 5;
	this->Dex = 10;
	this->Agi = 10;
	this->Vit = 5;
	this->name = name;
	this->level = 1;
	this->xp = 0;
	this->xpNext = level * 1000;
	this->hp = 10;
	this->hpMax = 10;
	this->damageMin = 2;
	this->damageMax = 4;
	this->gold = 100;
}
void Player::initializeCrusader(const string name)
{
	this->xCoord = 0;
	this->yCoord = 0;

	this->Pow = 5;
	this->Dex = 5;
	this->Agi = 5;
	this->Vit = 10;
	this->name = name;
	this->level = 1;
	this->xp = 0;
	this->xpNext = level * 1000;
	this->hp = 20;
	this->hpMax = 20;
	this->damageMin = 2;
	this->damageMax = 4;
	this->gold = 100;
}

void Player::showStats() const
{
	cout << "=== Player Info ===" << endl;
	cout << "Name: " << this->name << endl;
	cout << "Level: " << this->level << endl;
	cout << "Xp: " << this->xp << endl;
	cout << "Xp to level up: " << this->xpNext << endl;
	cout << "Hp: " << this->hp << "/" << this->hpMax << endl;
	cout << "Power: " << this->Pow << endl;
	cout << "Dexterity: " << this->Dex << endl;
	cout << "Agility: " << this->Agi << endl;
	cout << "Vitality: " << this->Vit << endl;
	cout << "Damage: " << this->damageMin << "-" << this->damageMax << endl;
	cout << "Gold: " << this->gold << endl;
}

void Player::heal()
{
	this->hp = this->hpMax;
	cout << "Your character is well rested, 100% HP" << endl;
}

void Player::setxcoord(int xcoord)
{
	xCoord = xcoord;
}

void Player::setycoord(int ycoord)
{
	yCoord = ycoord;
}

void Player::attack(Enemy* target)
{
	int physicalDamage = (this->getPow() + weapon.getdamage()) - (enemy.getVit() + armor.getdefence());
	cout << this->getPow();
}
