#include "Enemy.h"

Enemy::Enemy(int level)
{
	this->level = level;
	this->hpMax = level * 10;
	this->hp = this->hpMax;
	this->damageMin = 0;
	this->damageMax = 0;
	this->Pow = 0;
	this->Dex = 0;
	this->Agi = 0;
	this->Vit = 0;
	this->goldDrop = 0;
	this->xpToGive = 0;
}

Enemy::~Enemy()
{
}

void Enemy::initializeGoblin()
{
	this->level = level;
	this->hpMax = level * 10;
	this->hp = this->hpMax;
	this->damageMin = level * 1;
	this->damageMax =  level * 2;
	this->Pow = 8;
	this->Dex = 5;
	this->Agi = 5;
	this->Vit = 5;
	this->goldDrop = 10;
	this->xpToGive = 100;
}

void Enemy::initializeOgre()
{
	this->level = level;
	this->hpMax = level * 11;
	this->hp = this->hpMax;
	this->damageMin = level * 1;
	this->damageMax = level * 2;
	this->Pow = 5;
	this->Dex = 5;
	this->Agi = 5;
	this->Vit = 8;
	this->goldDrop = 50;
	this->xpToGive = 250;
}

void Enemy::initializeOrc()
{
	this->level = level;
	this->hpMax = level * 12;
	this->hp = this->hpMax;
	this->damageMin = level * 1;
	this->damageMax =level * 2;
	this->Pow = 10;
	this->Dex = 10;
	this->Agi = 10;
	this->Vit = 10;
	this->goldDrop = 100;
	this->xpToGive = 500;
}

void Enemy::initializeOrcLord()
{
	this->level = level;
	this->hpMax = level * 13;
	this->hp = this->hpMax;
	this->damageMin = level * 2;
	this->damageMax = level * 3;
	this->Pow = 20;
	this->Dex = 20;
	this->Agi = 20;
	this->Vit = 20;
	this->goldDrop = 1000;
	this->xpToGive = 1000;
}
void Enemy::sethp(int mhp)
{
	hp = mhp;
}
string Enemy::getAsString() const
{
	return "Level: " + to_string(this->level) + "\n" +
		"Hp: " + to_string(this->hp) + "/" + to_string(this->hpMax) + "\n" +
		"Damage: " + to_string(this->damageMin) + "-" + to_string(this->damageMax) + "\n" +
		"Gold Drop: " + to_string(this->goldDrop) + "\n";

}

//void Enemy::attack(Player* target)
//{
//}