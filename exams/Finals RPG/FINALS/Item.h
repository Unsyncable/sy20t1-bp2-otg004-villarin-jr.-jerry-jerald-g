#pragma once
#include <string>
#include <iostream>

using namespace std;
class Item
{
public:
	Item(string name, int price);
	virtual ~Item();

	inline const string& getName() const { return this->name; }
	inline const int& getPrice() const { return this->price; }


private:
	string name;
	int price;

};

