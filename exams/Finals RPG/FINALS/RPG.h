#pragma once
#include "Function.h"
#include "Player.h"
#include "Enemy.h"
#include <iomanip>
#include <time.h>

using namespace std;
class RPG
{
public:
	RPG();
	virtual ~RPG();

	void startGame();
	void mainMenu();
	void initializeBattle();
	void encounter();
	void Travel(Player);

	inline bool getplaying() const { return this->playing; }

private:
	int choice;
	bool playing;

	Player player;
	Enemy enemy;
};

