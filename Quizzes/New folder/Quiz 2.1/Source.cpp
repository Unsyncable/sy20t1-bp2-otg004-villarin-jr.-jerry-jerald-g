#include <iostream>
#include <time.h>
#include <string>
#include "Node.h"


using namespace std;

Node* randomizer(Node* active, int activeSoldiers)
{
	int randomNumber = rand() % activeSoldiers + 1;

	for (int i = 0; i < randomNumber; i++)
	{
		active = active->next;
	}
	return active;
}
void printActiveSoldiers(Node* active, int activeSoldiers)
{
	cout << "Active soldiers: " << endl;
	for (int i = 0; i < activeSoldiers; i++)
	{
		cout << active->name << endl;
		active = active->next;
	}

}
Node* passingCloak(Node* active, int randomNumber)
{
	Node* toDelete = new Node;
	for (int i = 0; i < randomNumber; i++)
	{
		toDelete = active;
		active = active->next;
	}

	toDelete->next = active->next;
	delete active;
	toDelete = toDelete->next;

	
	return toDelete;
}

	



int main()
{
	srand(time(NULL));
	int activeSoldiers = 5;

	Node* soldier1 = new Node;
	soldier1->name = "Alliser";
	Node* soldier2 = new Node;
	soldier2->name = "Janos";
	Node* soldier3 = new Node;
	soldier3->name = "Othell";
	Node* soldier4 = new Node;
	soldier4->name = "Sam";
	Node* soldier5 = new Node;
	soldier5->name = "Snow";

	Node* head = soldier1;

	soldier1->next = soldier2;
	soldier2->next = soldier3;
	soldier3->next = soldier4;
	soldier4->next = soldier5;
	soldier5->next = soldier1;


	head = randomizer(head, activeSoldiers);
	int roundCounter = 1;

	while(activeSoldiers > 1)
	{
		cout << "==========================================" << endl;
		cout << "ROUND: " << roundCounter << endl;
		cout << "==========================================" << endl;
		int randomNumber = rand() % activeSoldiers + 1;
		cout << head->name << " has drawn " << randomNumber << endl;
		printActiveSoldiers(head, activeSoldiers);
		head = passingCloak(head, randomNumber);
		system("pause");
		system("cls");
		activeSoldiers--;
		roundCounter++;
	}
	cout << "==========================================" << endl;
	cout << "FINAL RESULT: "<< endl;
	cout << "==========================================" << endl;
	cout << head->name << " will go to seek for reinforcements." << endl;

	system("pause");
	return 0;

}