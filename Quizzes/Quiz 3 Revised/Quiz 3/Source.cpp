#include <iostream>
#include <time.h>
#include <string>
#include "Unit.h"
#include "Source.h"

using namespace std;

void choosetype(string pName, int type, Unit * p1)
{
	cout << "Please enter a name for your character: " << endl;
	cin >> pName;
	string types[3] = { "Warrior", "Assasin", "Mage" };
	cout << "Please choose your class "<< pName << " :" << endl;
	cout << "[1] Warrior" << endl << "Character bonus: " << endl << "Power: + 3 points" << endl << "Vitality + 3 points" << endl << endl;
	cout << "[2] Assasin" << endl << "Character bonus: " << endl << "Agility + 3 points" << endl << "Dexterity + 3 points" << endl << endl;
	cout << "[3] Mage" << endl << "Character bonus: " << endl << "Power + 5 points" << endl << endl;
	cin >> type;
	if (type == '1')
	{
		cout << "You have chosen: " << types[0] << endl;
	}
	if (type == '2')
	{
		cout << "You have chosen: " << types[1] << endl;
	}
	if (type == '3') 
	{
		cout << "You have chosen: " << types[2] << endl;
	}
	system("cls");
}

void enemyspawner(string* eName, int eType, Unit* e1)
{
	srand(time(NULL));
	
	int i = 0;
	i = rand() % 3;
	string etypes[3] = { "Enemy Warrior", "Enemy Assasin", "Enemy Mage" };
	string* name = &etypes[i];

	etypes[i];
	if (i == 0)
	{
		cout << "Opponent: " << endl << etypes[0] << endl;

	}
	if (i == 1)
	{
		cout << "Opponent: " << endl << etypes[1] << endl;

	}
	if (i == 2)
	{
		cout << "Opponent: " << endl << etypes[2] << endl;

	}
	system("pause");
}
int main()
{
	string Name;
	string* ptr = &Name;
	int type{};
	int round = 1;
	Unit* p1 = new Unit();
	Unit* e1 = new Unit();
	choosetype(Name, type, p1);
	enemyspawner(&Name, type, e1);
	//default values
	e1->setAgi(3);
	e1->setDex(3);
	e1->setPow(3);
	e1->setVit(3);
	e1->setHp(3);

	p1->setAgi(20);
	p1->setDex(20);
	p1->setPow(20);
	p1->setVit(20);
	p1->setHp(20);



	while (p1->getHp() > 0)
	{
		cout <<"Enemy Hp is: " << e1->getHp() << endl;
		cout <<"Player Hp is: " <<p1->getHp() << endl;
		p1->attack(e1);
		
		if (round > 1)
		{
			e1->statScaler();
		}
		round++;

		system("pause");
		system("cls");
	}

}
