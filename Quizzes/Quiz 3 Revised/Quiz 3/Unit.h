#pragma once
#include <iostream>
#include <string>

using namespace std;

class Unit
{
public:
	string getType();
	string setType(string type);

	string getName();

	int getHp();
	void setHp(int value);

	int getPow();
	void setPow(int value);

	int getVit();
	void setVit(int value);

	int getAgi();
	void setAgi(int value);

	int getDex();
	void setDex(int value);

	int getDmg();
	void setDmg(int value);

	int getHit();
	void setHit(int value);

	void attack(Unit* target);
	
	void statScaler();
	
private:
	string mType;
	string mName;
	int mHp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
	int mDmg;
	int mHit;
};

