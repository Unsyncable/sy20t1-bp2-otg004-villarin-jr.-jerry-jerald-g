#include "Unit.h"
#include <iostream>

using namespace std;

string Unit::getType()
{
	return mType;
}

string Unit::setType(string type)
{
	return type;
}

string Unit::getName()
{
	return mName;
}

int Unit::getHp()
{
	return mHp;
}
void Unit::setHp(int value)
{
	mHp = value;
}

int Unit::getPow()
{
	return mPow;
}

void Unit::setPow(int value)
{
	mPow = value;
}

int Unit::getVit()
{
	return mVit;
}

void Unit::setVit(int value)
{
	mVit = value;
}

int Unit::getAgi()
{
	return mAgi;
}

void Unit::setAgi(int value)
{
	mAgi = value;;
}

int Unit::getDex()
{
	return mDex;
}

void Unit::setDex(int value)
{
	mDex = value;;
}

int Unit::getDmg()
{
	return mDmg;
}

void Unit::setDmg(int value)
{
	mDmg = value;
}

int Unit::getHit()
{
	return mHit;
}

void Unit::setHit(int value)
{
	mHit = value;
}

void Unit::attack(Unit* target)
{
	cout << "==================================================" << endl;
	int bonusdamage = 150;
	int totaldamage = (this->mPow - target->getVit());
	target->mHp-= totaldamage;
	cout << this->mName << "Damage Dealt: " << totaldamage << endl;
	cout << "==================================================" << endl;
}

void Unit::statScaler()
{
		this->setHp(getHp() + 5);
		this->setVit(getVit() + 5);
		this->setAgi(getAgi() + 5);
		this->setDex(getDex() + 5);
		this->setPow(getPow() + 5);
} 

