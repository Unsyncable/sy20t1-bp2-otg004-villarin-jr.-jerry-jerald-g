#include "Unit.h"
string Unit::getType()
{
	return mType;
}

string Unit::getName()
{
	return mName;
}

int Unit::getHp()
{
	return mHp;
}
void Unit::setHp(int value)
{
	mHp = value;
}

int Unit::getPow()
{
	return mPow;
}

void Unit::setPow(int value)
{
	value = mPow;
}

int Unit::getVit()
{
	return mVit;
}

void Unit::setVit(int value)
{
	value = mVit;
}

int Unit::getAgi()
{
	return mAgi;
}

void Unit::setAgi(int value)
{
	value = mAgi;
}

int Unit::getDex()
{
	return mDex;
}

void Unit::setDex(int value)
{
	value = mDex;			
}

int Unit::getDmg()
{
	return mDmg;
}

void Unit::setDmg(int value)
{
	value = mDmg;
}

int Unit::getHit()
{
	return mHit;
}

void Unit::setHit(int value)
{
	value = mHit;
}

void Unit::attack(Unit* target)
{
	int damage = (mPow - target->mVit) * 150;
}

