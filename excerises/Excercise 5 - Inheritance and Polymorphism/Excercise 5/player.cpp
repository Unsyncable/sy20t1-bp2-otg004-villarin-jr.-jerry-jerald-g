#include "player.h"


player::player()
{
	this->name = "";
	this->mhp = 0;
	this->mpow = 0;
	this->mdex = 0;
	this->magi = 0;
	this->mvit = 0;
}
void player::initialize(const string name)
{

	this->mpow = 5;
	this->mdex = 5;
	this->magi = 5;
	this->mvit = 5;
	this->name = name;
	this->mhp = 10;
}
void player::sethp(int hp)
{
	mhp = hp;
}

void player::setpow(int pow)
{
	mpow = pow;
}

void player::setvit(int vit)
{
	mvit = vit;
}

void player::setdex(int dex)
{
	mdex = dex;
}

void player::setagi(int agi)
{
	magi = agi;
}



void player::randomstat()
{
	srand(time(NULL));
	int spellcast = rand() % 5;
	switch (spellcast)
	{
	case 0:
		cout << "You casted Heal" << endl;
		this->sethp(mhp + 10);
		break;
	case 1:
		cout << "You casted Might" << endl;
		this->setpow(mpow + 2);
		break;
	case 2:
		cout << "You casted Iron Skin" << endl;
		this->setvit(mvit + 2);
		break;
	case 3:
		cout << "You casted Concentration" << endl;
		this->setdex(mdex + 2);
		break;
	case 4:cout << "You casted Haste" << endl;
		this->setagi (magi + 2);

		break;

	}
}

void player::showStats()
{
	cout << endl;
	cout << "=== Player Info ===" << endl;
	cout << "Name: " << this->name << endl;
	cout << "Hp: " << this->mhp << endl;
	cout << "Power: " << this->mpow << endl;
	cout << "Dexterity: " << this->mdex << endl;
	cout << "Agility: " << this->magi << endl;
	cout << "Vitality: " << this->mvit << endl;
}