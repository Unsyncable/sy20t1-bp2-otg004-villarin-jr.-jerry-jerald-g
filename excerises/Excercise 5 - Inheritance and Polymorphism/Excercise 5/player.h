#pragma once
#include <iostream>
#include <string>
#include <time.h>


using namespace std;
class player
{
public:
	player();
	void randomstat();
	void showStats();

	void initialize(const string name);

	inline const string& getName() const { return this->name; }
	inline const int& gethp() const { return this->mhp; }
	inline const int& getpow() const { return this->mpow; }
	inline const int& getvit() const { return this->mvit; }
	inline const int& getdex() const { return this->mdex; }
	inline const int& getagi() const { return this->magi; }

	void sethp(int hp);
	void setpow(int pow);
	void setvit(int vit);
	void setdex(int dex);
	void setagi(int agi);
private:
	string name;
	int mhp;
	int mpow;
	int mvit;
	int mdex;
	int magi;			
};

