#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std; 

void printInv(const vector<string>& items)
{
	for (int i = 0; i < items.size(); i++)
	{
		cout << items[i] << endl;
	}
}
void populateInv(vector<string>& items)
{
	string stuff[4] = { "Redpotion", "Elixir", "EmptyBottle","BluePotion" };
	for (int i = 0; i < 10; i++)
	{
		items.push_back(stuff[rand() % 4]);		
	}
}
int countInv(vector<string> items, string itemInput)
{
	int counter = 0;
	for (int i = 0; i < items.size(); i++)
	{
		if (items[i] == itemInput)
		{
			counter++;
		}
	}
	return counter;
}
void DeleteItem(vector<string>& items, string itemInput)
{
	for (int i = 0; i < items.size(); i++)
	{
		if (items[i] == itemInput)
		{
			items[i].erase();
		}
	}
}
int main()
{
	
	srand(time(NULL));
	vector<string> items;
	populateInv(items);
	printInv(items);

	string input;
	cout << "Enter an item: " << endl;
	cin >> input;
	int countItem = countInv(items, input);
	cout << "You have " << " " << countItem << " " << "of " << input << endl;

	cout << "Please enter an item you want to remove: " << endl;
	cin >> input;
	DeleteItem(items, input);
	printInv(items);


	system("pause");
	return 0;
}