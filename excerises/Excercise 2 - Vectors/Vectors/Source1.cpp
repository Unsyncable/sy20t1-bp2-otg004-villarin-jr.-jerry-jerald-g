Skip to content
GitLab
Projects
Groups
Snippets
Help
S
STD Vector
Project overview
Repository
Files
Commits
Branches
Tags
Contributors
Graph
Compare
Locked Files
Issues
0
Merge Requests
0
Requirements
CI / CD
Security & Compliance
Operations
Packages & Registries
Analytics
Wiki
Snippets
Members

Close sidebar
Open sidebar
C++
STD Vector
Repository
master
std - vector
StdVector.cpp
Deng Rodil's avatar
Organized folder
Deng Rodil authored 3 months ago
172624cc
StdVector.cpp 3.36 KB
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
47
48
49
50
51
52
53
54
55
56
57
58
59
60
61
62
63
64
65
66
67
68
69
70
71
72
73
74
#include <iostream>
#include <string>
#include <time.h>
#include <vector> // Include this to use vector
using namespace std; // vector is under std:: namespace
// We always add a reference to the vector to optimize memory.
// Constant reference to a vector means you can only read the contents of the vector. You are not allowed to call any function that mutates the contents of the vector (push_back, erase, clear, etc)
void printVector(const vector<int>& vector)
{
	for (int i = 0; i < vector.size(); i++)
	{
		cout << vector[i] << endl;
	}
}
// Same as 'printVector' but this time we need 'write access' to the vector. This means we can use functions that mutates the contents of the vector (push_back, erase, clear, etc)
void populateVector(vector<int>& vector)
{
	for (int i = 0; i < 5; i++)
	{
		vector.push_back(rand() % 1000);
	}
}
int main()
{
	// Seed RNG
	srand(time(NULL));
	// Initialize the vector. Always initialize it as a value-type.
	vector<int> numbers;
	// Populate the vector with 5 random integer
	numbers.push_back(rand() % 1000); // This inserts the random number to index 0 (since the list is still empty)
	numbers.push_back(rand() % 1000); // Index 1. Remember, that push_back() automatically inserts the new element at the end of the list. No need for index
	numbers.push_back(rand() % 1000); // Index 2
	numbers.push_back(rand() % 1000); // Index 3
	numbers.push_back(rand() % 1000); // Index 4. I can use a loop for this but didn't so that it's easier for you to digest.
	// Let's print index 2. You can do this in 2 ways. Either way yields the same result
	cout << "Printing index 2..." << endl;
	cout << numbers[2] << " - using bracket operator" << endl; // Traditional bracket operator. Similar to array.
	cout << numbers.at(2) << " - using at()" << endl; // Using the at() function of vector.
	cout << endl << endl;
	// Let's try to modify the value of index 2. You can also do this in 2 ways. Either way yields the same result
	cout << "Changing value of index 2 to 999..." << endl << endl;
	numbers[2] = 999; // Traditional bracket operator. Similar to array.
	numbers.at(2) = 999; // at() can also be used to set the value within the index.
	// Let's try to print all the elements of an array by iterating over it. This method uses the traditional for-loop. You don't need to store the size of the vector in a separate variable. You can directly use size() which returns the number of elements in the vector. Cool, right?
	cout << "Printing the array using bracket operator..." << endl;
	for (int i = 0; i < numbers.size(); i++)
	{
		cout << numbers[i] << endl;
	}
	cout << endl << endl;
	// Let's try to remove index 2. This syntax is kinda confusing since it needs an "iterator". You always have to include the .begin() and add the index to it. Unfortunately, there's no other way.
	cout << "Removing element at index 2..." << endl << endl;
	numbers.erase(numbers.begin() + 2); // removes index 2
	// Populate the vector with 5 random values. We'll pass the vector to the function as reference since we want the changes (push_back) to the vector to reflect back to the 'numbers' vector in this scope.
	populateVector(numbers);
	// Let's print the vector again. This time, we'll pass the vector to the function
	cout << "Printing the vector using 'iterator'. Also, we'll use a function so you'll know how to pass vectors around..." << endl;
	printVector(numbers);
	system("pause");
	return 0;
}