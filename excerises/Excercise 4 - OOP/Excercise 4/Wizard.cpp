#include "Wizard.h"


void Wizard::attack(Wizard* target)
{
	int physicalDamage = rand() % ((mMaxPhysicalDamage + 1) - mMinPhysicalDamage) + mMinPhysicalDamage;
	int generatedMp = rand() % (21 - 10) + 10;
	target->mHp -= physicalDamage;
	mMp += generatedMp;
	cout << this->mName << " attacks " << target->mName << endl;
	cout << this->mName << " dealt " << physicalDamage << " and has gained " << generatedMp << " mana points! " << endl;
}
