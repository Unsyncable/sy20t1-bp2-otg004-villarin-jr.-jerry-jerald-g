#include "Spell.h"
#include "Wizard.h"
#include <iostream>


void Spell::activate(Wizard* target, Wizard* caster)
{
	int magicalDamage = rand()  % ((mMaxSpellDamage + 1) - mMinSpellDamage) + mMinSpellDamage;
	target->mHp -= magicalDamage;
	caster->mMp -= mMpCost;
	cout << caster->mName << " has casted " << mSpellName <<" and dealt " << magicalDamage <<endl;
}
