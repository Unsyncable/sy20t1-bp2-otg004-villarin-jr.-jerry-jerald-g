#include <iostream>
#include <time.h>
#include <string>
#include "Wizard.h"
#include "Spell.h"

using namespace std;

int main()
{
	srand(time(0));

	Wizard* w1 = new Wizard();
	w1->mName = "Kael";
	Wizard* w2 = new Wizard();
	w2->mName = "Lina";

	while (w1->mHp > 0 && w2->mHp > 0)
	{
		if (w1->mMp >= 50)
		{
			Spell* s1 = new Spell();
			s1->mSpellName = "Deafening Blast";

			s1->activate(w2, w1);

			delete s1;
		}
		else if (w2->mMp >= 50)
		{
			Spell* s2 = new Spell();
			s2->mSpellName = "Laguna Blade";

			s2->activate(w1, w2);


			delete s2;
		}
		cout << "=================================" << endl;
		cout << "WELCOME TO WIZARD 1 v 1" << endl;
		cout << "=================================" << endl;
		cout << w1->mName << endl;
		cout << " Hp: " << w1->mHp << endl << " Mp: " << w1->mMp << endl;
		cout << w2->mName << endl;
		cout << " Hp: " << w2->mHp << endl << " Mp: " << w2->mMp << endl;
		w1->attack(w2);
		w2->attack(w1);
		
		system("pause");
		system("cls");
	}
}
