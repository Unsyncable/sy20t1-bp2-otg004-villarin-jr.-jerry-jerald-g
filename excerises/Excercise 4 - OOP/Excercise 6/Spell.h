#pragma once
#include <string>
#include <iostream>

using namespace std;

class Wizard;

class Spell
{
public:

	string mSpellName;
	int mMinSpellDamage = 40;
	int mMaxSpellDamage = 60;
	int mMpCost = 50;

	void activate(Wizard* target, Wizard* caster);
};
