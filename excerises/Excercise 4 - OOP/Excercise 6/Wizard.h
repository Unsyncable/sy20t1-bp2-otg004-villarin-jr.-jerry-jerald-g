#pragma once
#include <string>
#include <iostream>

using namespace std;

class Wizard 
{
public:

	string mName;
	int mHp = 250;
	int mMp = 0;
	int mMinPhysicalDamage = 10;
	int mMaxPhysicalDamage = 15;
	
	void attack(Wizard* target);
};

