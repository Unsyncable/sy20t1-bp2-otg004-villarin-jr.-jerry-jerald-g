#include <iostream>	
#include <time.h>
#include <string>
#include <vector>

using namespace std;

typedef struct
{
		string name;
		int gold;
}itemStruct;
itemStruct* generateRandomItem()
{
	srand(time(NULL));
	itemStruct* item = new itemStruct();
	string itemName[5]{ "Mithril Ore", "Sharp Talon", "Thick Leather", "Jellopy", "Cursed Stone" };
	for (int i = 0; i <= 5; i++)
	{
		i = rand() % 5;
		item->name = itemName[i];

		if (i == 4)
		{
			cout << "This is a cursed stone!" << endl;
			break;
		}
		else if (i == 0)
		{
			item->gold = 100;
		}
		else if (i == 1)
		{
			item->gold = 75;
		}
		else if (i == 2)
		{
			item->gold = 50;
		}
		else if (i == 3)
		{
			item->gold = 25;
		}
		return item;
	}

}
void enterDungeon(int& gold)
{
	itemStruct* randItem = generateRandomItem();
	int money = 50;
	int multiplier = 1;
	cout << "Do  you still want to keep going?" << endl << "[1] Yes" << endl << "[2] No " << endl;
	char choice;
	cin >> choice;
	if (choice == '1')
	{
		while (money >= 500);
		{
			cout << "Do  you still want to keep going?" << endl << "[1] Yes" << endl << "[2] No " << endl;
			char choice;
			cin >> choice;

			cout << "Item: " << randItem->name << endl;
			cout << "Cost: " << randItem->gold * multiplier << endl;
			multiplier++;
			randItem->gold + money;
		}
	}
}

int main()
{
	int coins;
	int mainCoins = 50;
	char answer;
	cout << "Welcome to the dungeon!" << endl << "Would you like to enter?" << endl << "The fee is 25 coins" << endl << "[1] Yes" << endl << "[2] No" << endl;
	cin >> answer;
	if (answer == '1')
	{
		mainCoins - 25;
		enterDungeon(mainCoins);
	}
	if (answer == '2')
	{
		cout << "Game over" << endl;

	}




	system("pause");
	return 0;
}