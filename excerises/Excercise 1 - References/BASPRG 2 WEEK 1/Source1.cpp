#include <iostream>
#include <time.h>
#include <string>

using namespace std;

void coinloss(int charactergold, int& characterbet)
{
	characterbet -= charactergold;
}
void coinwin(int charactergold, int& characterbet)
{
	characterbet += charactergold;
}
//    int coinloss(int charactergold, int& characterbet)
//  {
	  //characterbet -= charactergold;
	  //return characterbet;
//  }
//     int coinwin(int charactergold, int& characterbet)
//  {
	   //characterbet += charactergold;
	   //return characterbet;
//  }
void roll(int& characterdice1, int& characterdice2, int& computerdice1, int& computerdice2)
{
	srand(time(NULL));
	characterdice1 = rand() % 6 + 1;
	characterdice2 = rand() % 6 + 1;
	computerdice1 = rand() % 6 + 1;
	computerdice2 = rand() % 6 + 1;
}

int main()
{

	int playergold = 1000;
	int playerroll;
	int playerroll2;
	int airoll;
	int airoll2;

	while (true)
	{
		cout << "Please enter your desired bet:" << endl;
		int playerbet;
		cin >> playerbet;
		cout << "Player has entered: " << playerbet << endl;

		if (playerbet != 0 && playerbet < playergold || playergold == playerbet)
		{
			roll(playerroll, playerroll2, airoll, airoll2);
			int sumplayerroll = playerroll + playerroll2;
			int sumairoll = airoll + airoll2;
			cout << "Player rolled a total of: " << sumplayerroll << endl;
			cout << "Ai rolled a total of: " <<sumairoll << endl;

			if (sumplayerroll == 2)
			{
				cout << " Player rolled snake eyes!" << endl;
				cout << "Player wins!" << endl;
				coinwin(playerbet * 3, playergold);
				cout << "Player gold is now " << playergold << endl;

			}
			else if (sumairoll == 2)
			{
				cout << "Ai got snake eyes!" << endl;
				cout << "Player lost!" << endl;
				coinloss(playerbet, playergold);
				cout << "Player gold is now " << playergold << endl;
			}
			else if (sumplayerroll > sumairoll)
			{
				cout << "Player has a higher sum!" << endl;
				cout << "Player Wins!" << endl;
				coinwin(playerbet, playergold);
					cout << " Player gold is now: " << playergold << endl;
			}
			else if (sumplayerroll < sumairoll)
			{
				cout << "Ai has a higher sum!" << endl;
				cout << "Player lost!" << endl;
				coinloss(playerbet, playergold);
				cout << "Player gold is now: " << playergold << endl;
			}
			else if (sumplayerroll == sumairoll)
			{
				cout << "Player and Ai have the same sum!" << endl;
				cout << "We have a draw!" << endl;
				cout << "Player gold is stil: " << playergold << endl;
			}
			if (playergold == 0)
			{
				cout << "Player has no gold remaining" << endl;
				cout << "Player lost!" << endl;
				break;
			}
		}
		else
		{
			cout << "Player entered an invalid amount!" << endl;
			cout << "Try a lower amount!" << endl;

		}
	}
	system("pause");
	return 0;
}
